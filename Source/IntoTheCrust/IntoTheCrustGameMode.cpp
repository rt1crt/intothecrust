// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "IntoTheCrustGameMode.h"
#include "IntoTheCrustCharacter.h"
#include "UObject/ConstructorHelpers.h"

AIntoTheCrustGameMode::AIntoTheCrustGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
