// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IntoTheCrustGameMode.generated.h"

UCLASS(minimalapi)
class AIntoTheCrustGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AIntoTheCrustGameMode();
};



